﻿using System;
namespace _3._1_Homework_DelegateEvents_Zverev.I.I
{
    public class MyFileFinder
    {
        private string _path;

        private bool _processing = true;

        CancellationTokenSource cancellationTokenSource;

        public MyFileFinder(string folderPath)
        {
            _path = folderPath;
            cancellationTokenSource = new CancellationTokenSource();
        }

        public event EventHandler FileFound;

        public async void SearchFilesAsync(IEnumerable<String> fileNames)
        {
            try
            {
                var token = cancellationTokenSource.Token;
                var allFilesPathFromDirectory = new List<string>();
                allFilesPathFromDirectory = await Task.Run(() => GetAllFullPathFromDirectoryAsync(token).Result);

                var allFilesNameFromDirectory = new List<string>();
                foreach (string filePath in allFilesPathFromDirectory)
                {
                    token.ThrowIfCancellationRequested();
                    allFilesNameFromDirectory.Add(Path.GetFileName(filePath));
                    Console.WriteLine($"В список файлов добавлен: {filePath}");
                    Thread.Sleep(5000);
                }

                foreach (string file in allFilesNameFromDirectory)
                {
                    token.ThrowIfCancellationRequested();
                    if (fileNames.Contains(file))
                    {
                        FileFound(file, EventArgs.Empty);
                    }
                }

                Console.WriteLine("Поиск файлов окончен");
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine("Операция поиска была прервана.");

                return;
            }
            catch
            { throw; }
            
        }

        public void StopProcessing() { cancellationTokenSource.Cancel(); }

        private async Task<List<String>> GetAllFullPathFromDirectoryAsync(CancellationToken token)
        {
            List<string> allFilesPathFromDirectory = new List<string>();
            allFilesPathFromDirectory = Directory.GetFiles(_path).ToList();

            while (allFilesPathFromDirectory.Count() == 0)
            {
                Console.WriteLine("начал цикл");
                token.ThrowIfCancellationRequested();
            };

            return await Task.FromResult(allFilesPathFromDirectory);

        }

    }
}

