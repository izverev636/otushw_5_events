﻿using System;
namespace _3._1_Homework_DelegateEvents_Zverev.I.I
{
    public static class IEnumerableExtension
    {

        public static T GetTheBiggestObj<T>(this IEnumerable<T> array, Func<T,T,bool> predicate)
        {
            T theBiggest = array.First();
            foreach (T obj in array)
            {
                if (predicate(obj,theBiggest))
                {
                    theBiggest = obj;
                }
            }
            return theBiggest;
        }

    }
}

