﻿using _3._1_Homework_DelegateEvents_Zverev.I.I;

Console.WriteLine("Запуск поиска наибольшего свойства в объектах.");
var person1 = new Person { Id = 1, Name = "Test1", Age = 20 };
var person2 = new Person { Id = 2, Name = "Test2", Age = 11 };
var person3 = new Person { Id = 3, Name = "Test3", Age = 30 };
var person4 = new Person { Id = 4, Name = "Test4", Age = 40 };
var person5 = new Person { Id = 5, Name = "Test5", Age = 50 };

var personCol = new List<Person> { person1, person2, person3, person4, person5 };
Func<Person, Person, bool> func = (Person p, Person o) => p.Age > o.Age;

Console.WriteLine(personCol.GetTheBiggestObj<Person>(func).Age.ToString());
Console.WriteLine("Конец поиска наибольшего свойства в объектах.");


Console.WriteLine("Запуск асинхронного поиска файлов в папке.");
var myFileFinder = new MyFileFinder(@"/Users/igor.zverev/Desktop/Csharp Porjects/Otus HomeWork/3.1 Homework_DelegateEvents_Zverev.I.I/test");
myFileFinder.FileFound += (object? sender, EventArgs e) => Console.WriteLine(sender);
Console.WriteLine("Начинаю процесс поиска файлов, с задержкой 5 секунд на каждом файле");
Console.WriteLine("Для оставноки процесса необходимо отправить: stop");
var fileNames = new List<String> { "test2", "kuku", "test3" };
myFileFinder.SearchFilesAsync(fileNames);

while (true)
{
    string cmd = Console.ReadLine();
    if (cmd == "stop")
    {
        myFileFinder.StopProcessing();
        break;
    }
    Console.WriteLine($"{cmd} - нет такой команды");
}
Console.WriteLine("Конец асинхронного поиска файлов в папке.");
Console.ReadLine();

