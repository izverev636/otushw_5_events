﻿using System;
namespace _3._1_Homework_DelegateEvents_Zverev.I.I
{
    public record Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }
    }
}

